Commerce Discount "Up to Order Products Subtotal"

This Drupal Commerce module creates a new type of Order Discount Offer: Fixed Amount Up to Products Subtotal.

The Fixed Amount offer provided by the Commerce Discount module applies to the entire order total,
and thus will be applied to non-product line items as well, such as Sales Tax and Shipping.

As a result the customer may get free shipping on the low subtotal orders, which may not be what the merchant wanted.

This module calculates the Products Subtotal before applying the discount,
effectively excluding non-product items and making sure they will be paid for.
