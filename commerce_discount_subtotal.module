<?php

/**
 * @file
 * Defines the discount offer bundle 'Up to Order Products Subtotal'.
 */

/**
 * Implements hook_commerce_discount_offer_type_info().
 */
function commerce_discount_subtotal_commerce_discount_offer_type_info() {
  $types = array();
  $types['subtotal_up_to_fixed_amount'] = array(
    'label' => t('Up to specified @currency amount off the Order Subtotal', array('@currency' => commerce_currency_get_symbol(variable_get('commerce_default_currency', 'USD')))),
    'action' => 'commerce_discount_subtotal_up_to_fixed_amount',
    'entity types' => array('commerce_order'),
  );

  return $types;
}

/**
 * Rules action: Apply discount to Order Subtotal up to fixed amount .
 */
function commerce_discount_subtotal_up_to_fixed_amount(EntityDrupalWrapper $order_wrapper, $discount_name) {

  if ($order_wrapper->type() != 'commerce_order') {
    return;
  }

  $discount_wrapper = entity_metadata_wrapper('commerce_discount', $discount_name);
  $discount_price = $discount_wrapper->commerce_discount_offer->commerce_fixed_amount->value();
  $discount_price['amount'] = -$discount_price['amount'];

  // Exit if the wrapper doesn't have a commerce_discounts property.
  if (!isset($order_wrapper->commerce_discounts)) {
    return;
  }
  // Calculate products subtotal.
  $subtotal_amount = 0;
  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
    if (in_array($line_item_wrapper->getBundle(), commerce_product_line_item_types())) {
      $subtotal_amount += $line_item_wrapper->commerce_unit_price->amount->value();
    }
  }
  // If the discount will bring the order subtotal to less than zero, set the
  // discount amount so that it stops at zero.
  if (-$discount_price['amount'] > $subtotal_amount) {
    $discount_price['amount'] = -$subtotal_amount;
  }

  $delta = $order_wrapper->commerce_discounts->count();
  // Set reference to the discount.
  // @todo: It doesn't work with the wrapper.
  $order = $order_wrapper->value();
  $order->commerce_discounts[LANGUAGE_NONE][$delta]['target_id'] = $discount_wrapper->discount_id->value();

  module_load_include('inc', 'commerce_discount', 'commerce_discount.rules');
  // Modify the existing discount line item or add a new one if that fails.
  if (!commerce_discount_set_existing_line_item_price($order_wrapper, $discount_name, $discount_price)) {
    commerce_discount_add_line_item($order_wrapper, $discount_name, $discount_price);
  }

  // Update the total order price, for the next rules condition (if any).
  commerce_order_calculate_total($order);

}
